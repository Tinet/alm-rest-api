Gem::Specification.new do |s|
  s.name        = 'alm-rest-api'
  s.version     = '1.0.1'
  s.date        = '2017-02-27'
  s.summary     = "ALM!"
  s.description = "ALM REST API Integration for version 12"
  s.authors     = ["Xiaomeng Zheng", "Miguel Garcia"]
  s.email       = 'xiaomengzheng@gmail.com'
  s.files       = ["lib/alm-rest-api.rb", "lib/alm-rest-api/constants.rb", "lib/alm-rest-api/defect-fields.rb", "lib/alm-rest-api/entity.rb", "lib/alm-rest-api/response.rb", "lib/alm-rest-api/rest-connector.rb", "lib/alm-rest-api/value-lists.rb", "lib/alm-rest-api/test-fields.rb", "lib/alm-rest-api/testset-fields.rb", "lib/alm-rest-api/testrun-fields.rb"]
  s.homepage    = 'http://github.com/xiaomengzheng/alm-rest-api'

  s.add_runtime_dependency 'happymapper', '= 0.4.1'
end
