require 'happymapper'

module TestSetFields

  class Field
    include HappyMapper

    tag 'Field'
    attribute :name, String, :tag => 'Name'
    element :value, String, :tag => 'Value'
  end

  class Fields
    include HappyMapper

    tag 'Fields'
    has_many :fields, Field
  end

  class TestSet
    include HappyMapper

    tag 'Entity'
    has_one :fields, Fields
  end

end

   
