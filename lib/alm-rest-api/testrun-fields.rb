require 'happymapper'

module TestRunFields
  class Field
    include HappyMapper

    tag 'Field'
    attribute :name, String, :tag => 'Name'
    element :value, String, :tag => 'Value'
  end

  class Fields
    include HappyMapper

    tag 'Fields'
    has_many :fields, Field
  end

  class TestRun
    include HappyMapper

    tag 'Entity'
    has_one :fields, Fields
  end
end

